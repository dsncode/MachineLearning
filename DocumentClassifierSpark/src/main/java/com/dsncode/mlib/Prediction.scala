package com.dsncode.mlib

import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.ml.PipelineModel

object Prediction extends App{
  val conf = new SparkConf().setAppName("demo").setMaster("spark://dsvm:7077")
 val sc = new SparkContext(conf)
  sc.addJar("/home/daniel/git/machinelearning/DocumentClassifierSpark/target/DocumentClassifierSpark-0.0.1-SNAPSHOT.jar")
 val sqlContext = new SQLContext(sc)
 
 
 val model = PipelineModel.load("/data2/WORKSHOP/spark/class.model")
 println("LOADED !")
 
 val testTitle = "If Java is a “call-by-value” language then why is this happening?"

val testBody = 
 "Which gives the result: num = 1 num = 2 num = 3 The problem here, is that since Java is call by value then the num attribute of Node should have remained 0. I think that this is due to creation of an object T, which would have contained an object N. But there is no attribute in T to save a Node like object. So how is this happening? Any help is nefariously appreciated. Moon."

val testText = testTitle + testBody

val testDF = sqlContext
   .createDataFrame(Seq( (99.0, testText)))
   .toDF("Label", "Text")
   
   
   
val result = model.transform(testDF)
println(result)
val prediction = result.collect()(0)(6)
   .asInstanceOf[Double]

println("Prediction: "+ prediction)  
 
 sc.stop()
}