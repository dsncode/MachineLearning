package com.dsncode.mlib.token

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.feature.{HashingTF, IDF}
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD

object TFTokenizer extends App{
  val path = "/home/daniel/git/machinelearning/DocumentClassifierSpark/src/main"
  val conf = new SparkConf().setAppName("demo").setMaster("local");
 val sc = new SparkContext(conf)
  
 val documents: RDD[Seq[String]] = sc.textFile(path+"/document.txt")
  .map(_.split(" ").toSeq)

val hashingTF = new HashingTF()
val tf: RDD[Vector] = hashingTF.transform(documents)
tf.saveAsTextFile(path+"/tf.txt")
// While applying HashingTF only needs a single pass to the data, applying IDF needs two passes:
// First to compute the IDF vector and second to scale the term frequencies by IDF.
tf.cache()
val idf = new IDF().fit(tf)
val tfidf: RDD[Vector] = idf.transform(tf)

tfidf.saveAsTextFile(path+"/resources/idf.txt")

// spark.mllib IDF implementation provides an option for ignoring terms which occur in less than
// a minimum number of documents. In such cases, the IDF for these terms is set to 0.
// This feature can be used by passing the minDocFreq value to the IDF constructor.
val idfIgnore = new IDF(minDocFreq = 2).fit(tf)
val tfidfIgnore: RDD[Vector] = idfIgnore.transform(tf)
tfidfIgnore.saveAsTextFile(path+"/idfignore.txt")


sc.stop()
}